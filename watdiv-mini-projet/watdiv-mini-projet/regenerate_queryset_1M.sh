for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 10000 1 > testsuite/queries/100K/${qt2%.sparql-template}_100K.queryset ;
done;
