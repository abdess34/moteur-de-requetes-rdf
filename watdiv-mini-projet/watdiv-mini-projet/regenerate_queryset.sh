for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 1000 1 > testsuite/queries/10K/${qt2%.sparql-template}_10K.queryset ;
done;
